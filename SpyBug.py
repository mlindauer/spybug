__author__= "Marius Lindauer"
__version__ = "0.1.0"

import sys
import os
import inspect

cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile( inspect.currentframe() ))[0]))
cmd_folder = os.path.join(cmd_folder,"SpyBug")
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

from setup.cmd_parser import CmdParser
from setup.smac_parser import SmacParser
from random_runs.random_runner import RandomRunner

def main():
    
    #read command line parameters
    parser = CmdParser()
    args_ = parser.parse_cmd()
    
    smac_parser = SmacParser()
    algo_call, pcs, insts, cutoff = smac_parser.parse_scenario(args_.scenario, args_.seed)
    
    runner = RandomRunner(algo_call, insts, pcs, cutoff)
    runner.run(args_.n_runs, args_.time_limit)
    

if __name__ == "__main__":
    main()