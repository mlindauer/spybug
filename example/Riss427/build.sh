#!/bin/sh
# Norbert Manthey, 2014
#
# script to build the SAT solver Riss
#


cd code;
# build Riss with DRAT support
make clean
make rissRS ARGS="-DTOOLVERSION=427"
cp riss ../riss

# return to calling directory
cd ..
