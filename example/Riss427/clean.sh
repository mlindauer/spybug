#!/bin/sh
# Norbert Manthey, 2014
#
# script to clean the SAT solver Riss
#

# clean Riss
cd code;
make clean

# return to calling directory
cd ..

r, -f riss
