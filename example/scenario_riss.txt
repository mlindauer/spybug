cutoff_time = 5
instance_file = instances.txt
algo = python -u ../scripts/SATCSSCWrapper.py --mem-limit 3000 --script Riss427/rissWrapper.py --sat-checker ../scripts/SAT --sol-file .
paramfile = Riss427/riss427-CSSC.pcs
