#!/bin/python

import os
import numpy

scens = {}
for f in os.listdir("."):
    if not (f.startswith("CSSC14") and f.endswith(".log")):
        continue
    scen = "_".join(f.replace(".log","").split("_")[:-1])
    
    print(f)
    bugs = set()
    with open(f) as fp:
        for line in fp:
            if line.startswith("[INFO] Minimal bug config:"):
                line = line.replace("\n","").replace("[INFO] Minimal bug config: ","")
                bugs.add(line)
            elif line.startswith("[INFO] Minimal configuration was empty."):
                bugs.add("")
                
    bug_sizes = []
    for b in bugs:
        bug_sizes.append( (len(b.split(" ")) + 1) / 2 )
    
    scens[scen] = {"bs": bug_sizes, "b":bugs}
  
for scen, stats_dict in scens.iteritems():  
    print(scen)
    print("> Bugs: %d" %(len(stats_dict["b"])))        
    if len(stats_dict["b"]) > 0:
        print("> Average minimal bug size: %.0f +- %.0f" %(numpy.mean(stats_dict["bs"]), numpy.std(stats_dict["bs"])))
        
                