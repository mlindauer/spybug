#!/bin/python

import os

aclib_root = "/data/aad/aclib/"

RUNS = 4

def get_call(aclib_root, scen):
    scen_full = os.path.join(aclib_root, "scenarios/sat/", scen, "scenario.txt")
        
    for seed in range(RUNS):
        call = "python /home/lindauer/git/spybug/SpyBug.py --scen %s --n 10000 --time 172800 --seed %d --aclib 1> %s_%d.log 2>&1" %(scen_full, seed, scen, seed) 
        print(call)

for scen in os.listdir(os.path.join(aclib_root, "scenarios/sat/")):
    if scen.startswith("CSSC14_industrial_CSSC-CircuitFuzz"):
        if "_disc_" in scen:
            continue 
        
        get_call(aclib_root, scen)

    if scen.startswith("CSSC14_randomSAT_CSSC-3SAT1k"):
        if "_disc_" in scen:
            continue
        if "_clasp-3.0.4-p8" in scen or "minisat" in scen:
            continue
        
        get_call(aclib_root, scen)