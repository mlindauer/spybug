'''
Created on Oct 29, 2015

@author: lindauer
'''

import logging
import random
import time

from run_ta.ta_runner import TARunner
from bug_minimizer.bug_minimizer import BugMinimizer

class RandomRunner(object):
    '''
    randomly picks pairs of configurations and instances and runs them
    '''


    def __init__(self, algo_call, instances, pcs, cutoff):
        '''
        Constructor
        '''
        self.instances = instances
        self.pcs = pcs
        self.cutoff = cutoff
        
        self.runner = TARunner(algo_call, cutoff)
        
    def run(self, n, time_limit=None, bug_minimize=True):
        '''
            runs <n> random pairs of configuration and instance
        '''
        start_time = time.time()
        
        minimizer = BugMinimizer(self.runner, self.pcs)
        report_n = 1
        
        for it in xrange(n):
            
            if it+1 == report_n:
                logging.info("Iteration: %d (reported in an exponential schedule)"%(it+1))
                report_n *=2
            
            inst_ = random.choice(self.instances)
            try:
                specifics = inst_[1]
            except IndexError:
                specifics = "0"  # as used in SMAC
            inst_ = inst_[0]

            if it == 0:
                config = self.pcs.get_default_config_dict()
            else:
                config = self.pcs.get_random_config_vector() # returns an internal vector representation of a configuration
                config = self.pcs.convert_param_vector(config) # converts the vector to a dictionary representation: name -> value
            
            status, _, _, _, _ = self.runner.run(inst_, config, specifics)
            
            if status not in ["SAT", "UNSAT", "SUCCESS", "TIMEOUT"]:
                conf = " ".join(["-%s %s" %(head, value) for head, value in config.iteritems()])
                logging.info("We have probably found a bug in %s on %s in iteration %d" %(conf, inst_, it+1))
                
                if bug_minimize:
                    bug_inst = minimizer.minimize(config, inst_, specifics)
                    if bug_inst:
                        self.instances = filter(lambda x: x[0] != bug_inst, self.instances)
                        if not self.instances:
                            logging.error("No Instances left -- all instances were removed. Exit")
                            break
                        
            if time_limit and time.time() - start_time > time_limit - float(self.cutoff):
                logging.info("Terminated because of reaching wall_clock time limit. Exit")
                break
            
        if it == n-1:
            logging.info("Number of algorithm runs (%d) exhausted. Exit" %(it+1))
            