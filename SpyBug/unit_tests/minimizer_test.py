'''
Created on Nov 4, 2015

@author: manju
'''
import unittest
import logging

from bug_minimizer.bug_minimizer import BugMinimizer 
from run_ta.ta_runner import TARunner
from configspace.config_space import ConfigSpace

class Test(unittest.TestCase):


    def setUp(self):
        logging.basicConfig(level=logging.DEBUG)
        pass


    def tearDown(self):
        pass


    def test_minimize(self):
         
        pcs_fn = "files/bug.pcs"
        pcs = ConfigSpace(pcs_fn)
         
        ta_runner = TARunner(algo_call="python files/bug_wrapper.py", cutoff=300)
         
        bm = BugMinimizer(ta_runner, pcs)
         
        bm.minimize(config={'p1' : "1",
                            'p2' : "1",
                            'p3' : "1",
                            'p4' : "1",
                            },
                            inst_="blub", 
                            specifics = "SATISFIABLE")
        
    def test_minimize_with_conditionals(self):
        
        pcs_fn = "files/bug_w_cond.pcs"
        pcs = ConfigSpace(pcs_fn)
        
        ta_runner = TARunner(algo_call="python files/bug_wrapper.py", cutoff=300)
        
        bm = BugMinimizer(ta_runner, pcs)
        
        bm.minimize(config={'p1' : "1",
                            'p2' : "1",
                            'p3' : "1",
                            'p4' : "0",
                            },
                            inst_="blub", 
                            specifics = "SATISFIABLE")
        


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testMinimize']
    unittest.main()