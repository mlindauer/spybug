#!/bin/python

import sys

crashed = False

args = sys.argv[6:]
args = dict((name, value.strip("'")) for name, value in zip(args[::2], args[1::2]))

if sys.argv[1] == "pseudo_inst2":
    crashed = True

    
if crashed:
    print("Result for ParamILS: CRASHED, 1, 1, 1, 1")
else:
    print("Result for ParamILS: SAT, 1, 1, 1, 1")
        