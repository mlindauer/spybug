#!/bin/python

import sys

crashed = False

args = sys.argv[6:]
args = dict((name, value.strip("'")) for name, value in zip(args[::2], args[1::2]))

for idx, p in enumerate(sys.argv):
    if args["-p2"] == "1" and args["-p4"] == "1":
        crashed = True
    elif args["-p3"] == "1":
        crashed = True
    
if crashed:
    print("Result for ParamILS: CRASHED, 1, 1, 1, 1")
else:
    print("Result for ParamILS: SAT, 1, 1, 1, 1")
        