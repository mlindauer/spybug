'''
Created on Oct 29, 2015

@author: lindauer
'''

import argparse
import os
import logging

class CmdParser(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        
        
    def parse_cmd(self):
        '''
            parses command line options with argparse
            and returns its parsed object
        '''
        
        parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
        
        parser.add_argument("--scenario", required=True, help="scenario file in SMAC format")
        parser.add_argument("--n_runs", default=1000, type=int, help="maximal number of algorithm runs")
        parser.add_argument("--time_limit", default=3600, type=int, help="wallclock time limit [sec]")
        parser.add_argument("--seed", default=12345, type=int, help="random seed")
        parser.add_argument("--aclib", default=False, action="store_true", help="create symlink structure of aclib")
        
        parser.add_argument("--verbose", default="INFO", choices=["INFO", "DEBUG"], help="verbosity level")
        
        args_ = parser.parse_args()
        
        logging.basicConfig(level=args_.verbose, format="[%(levelname)s] %(message)s")
        
        if args_.aclib:
            self.create_aclib_links(args_.scenario)
        
        return args_
        
    def create_aclib_links(self, scen_fn):
        '''
            create symlinks to aclib structure based on scenario file name path
        '''
        aclib_root = os.path.sep.join(scen_fn.split(os.path.sep)[:-4])
        logging.debug(aclib_root)
        
        scenarios_path = os.path.join(aclib_root, "scenarios")
        algorithms_path = os.path.join(aclib_root, "target_algorithms")
        instances_path = os.path.join(aclib_root, "instances")

        configurators_path = os.path.join(aclib_root, "configurators")

        if not os.path.exists("./scenarios"):
            os.symlink(scenarios_path, "./scenarios")
        else:
            logging.warn("./scenarios existed in working directory. Assuming correct symlink...")

        if not os.path.exists("./target_algorithms"):
            os.symlink(algorithms_path, "./target_algorithms")
        else:
            logging.warn("./target_algorithms existed in working directory. Assuming correct symlink...")

        if not os.path.exists("./instances"):
            os.symlink(instances_path, "./instances")
        else:
            logging.warn("./instances existed in working directory. Assuming correct symlink...")

        if not os.path.exists("./configurators"):
            os.symlink(configurators_path, "./configurators")
        else:
            logging.warn("./configurators existed in working directory. Assuming correct symlink...")
        
        
        
