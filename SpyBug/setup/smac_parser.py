'''
Created on Oct 29, 2015

@author: lindauer
'''

from pysmac.utils.smac_input_readers import read_scenario_file
from pysmac.utils.smac_output_readers import read_instances_file
from configspace.config_space import ConfigSpace

class SmacParser(object):
    '''
    parses all files related to a smac scenario
    '''


    def __init__(self):
        '''
        Constructor
        '''
        
    def parse_scenario(self, fn, seed=12345):
        '''
            parses smac scenario file (fn)
            and all related files
        '''
        
        # parse scenario
        scen_dict = read_scenario_file(fn)
        
        #algo call
        algo_call = scen_dict["algo"]
        
        # parse pcs
        pcs = ConfigSpace(scen_dict["paramfile"], seed=seed)

        #parse training instances
        # pysmac docu: each element is a list where the first element is the instance name followed by additional 
        # information for the specific instance.
        instances = read_instances_file(scen_dict["instance_file"])

        # runtime cutoff
        cutoff = scen_dict["cutoff_time"]
        
        return algo_call, pcs, instances, cutoff