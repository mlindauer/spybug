'''
Created on Oct 29, 2015

@author: lindauer
'''

import copy
import logging

from configspace.config_space import ParameterType

class BugMinimizer(object):
    '''
        takes a configuration and tries to minimize the configuration as long as it still has a bug
    '''


    def __init__(self, ta_runner, pcs):
        '''
        Constructor
        '''
        
        self.ta_runner = ta_runner
        self.pcs = pcs
        
    def minimize(self, config, inst_, specifics):
        '''
            the categorical parameters of the minimal configuration will be added to the pcs as a forbidden clause
            Args:
                config: name -> value
                inst: string
                specifics: string
            Returns:
                instance name if the minimal configuration was empty
        '''
        
        all_params = config.keys()
        unsafe_params, config = self._identify_safe_params(all_params, config, inst_, specifics)
        logging.debug("Faulty parameters after backward pass: %s" %(unsafe_params))
        
        if not unsafe_params:
            self._empty_config(inst_)
            return inst_
        
        unsafe_params.reverse()
        unsafe_params, config = self._identify_safe_params(unsafe_params, config, inst_, specifics)
        logging.debug("Faulty parameters : %s" %(unsafe_params))
        
        if not unsafe_params:
            self._empty_config(inst_)
            return inst_
        
        unsafe_config = ["-%s %s" %(p, config[p]) for p in unsafe_params]
        logging.info("Minimal bug config: %s" %(" ".join(unsafe_config)))
                
        self._add_cat_as_forbidden(config, unsafe_params)
        
        return None
                
    def _empty_config(self, inst_):
        logging.info("Minimal configuration was empty.")
        logging.info("Algorithm probably crashes on this instance always: %s" %(inst_))
        logging.info("Removing instance from instance list.")
                
    def _identify_safe_params(self, params, config, inst_, specifics):
        unsafe_params = []
        for p in params:
            # try to replace <p> with its default value
            p_def = self.pcs.parameters[p].default
            p_config = copy.deepcopy(config)
            p_config[p] = p_def
            p_config = self._fix_active(p_config) # fix all active/inactive params after flipping them
            
            status, _, _, _, _ = self.ta_runner.run(inst_, p_config, specifics)
            
            if status not in ["SAT", "UNSAT", "SUCCESS", "TIMEOUT"]:
                config[p] = p_def
            else:
                unsafe_params.append(p)
        return unsafe_params, config
            
    def _fix_active(self, config):
        
        config = self.pcs.convert_param_dict(config)
        config = self.pcs._fix_active(config)
        config = self.pcs.convert_param_vector(config)
        return config
    
    def _add_cat_as_forbidden(self, config, unsafe_params):
        
        forbidden_clause = []
        for p in unsafe_params:
            if self.pcs.parameters[p].type == ParameterType.categorical:
                forbidden_clause.append([p,config[p]])
            else:
                logging.debug("Not added to forbidden: %s %s %s" %(p, self.pcs.parameters[p].type, config[p]))
        self.pcs.forbiddens.append(forbidden_clause)
        logging.info("New forbidden clause: %s" %(forbidden_clause))
        