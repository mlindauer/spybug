'''
Created on Jul 22, 2015

@author: manju
'''

import sys
from subprocess import Popen, PIPE
import logging

class TARunner(object):
    '''
      runs a target algorithm
    '''


    def __init__(self, algo_call, cutoff):
        '''
        Constructor
        '''
        
        self.algo_call = algo_call
        self.cutoff = cutoff
        
        
    def run(self, inst, config, specifics):
        '''
            runs target algorithm on <inst> instance and configuration <config>
            Args:
                inst: string
                config: parameter name -> value
        '''
        
        cmd = "%s %s %s %s 1234567890 12345 %s" %(self.algo_call, inst, specifics, self.cutoff, 
                                            " ".join(["-%s %s" %(head, value) for head, value in config.iteritems()]))
        
        logging.debug("Calling: %s" %(cmd))
         
        p = Popen(cmd, shell=True, stdout=PIPE, stdin=PIPE)
        stdout, stderr = p.communicate()
        
        for line in stdout.split("\n"):
            if line.startswith("Result for ParamILS:") or line.startswith("Result for SMAC:"):
                logging.debug(line)
                status, time, runlength, cost, seed = map(lambda x: x.strip(" "), line.split(":")[1].split(",")[:5])
                return status, time, runlength, cost, seed
          
        logging.error("No result line (\"Result for ParamILS\") found.")  
        return "CRASHED", 9999999999, 9999999999, 9999999999, 12345
        
                 
                 
        
        